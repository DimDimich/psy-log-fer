import React from 'react';
import PropTypes from 'prop-types';
import { IEntityData, IEntityID } from 'psy-log-models';
import DashboardCard from '../components/DashboardCard';
import CollectionMapping from '../../constants/CollectionMapping';

function DashboardCardList({items, collection}) {
  const title = collection && CollectionMapping.find(item => item.collection === collection).collection;

  if (items && items.length) {
    return (
      <>
        {items.map((item: IEntityData & IEntityID) =>
            <DashboardCard key={item.id} collection={collection} item={item} title={title}/>)}
      </>
    );
  } else {
    return (
      <>
        There is no items
      </>
    );
  }
}

DashboardCardList.propTypes = {
  items: PropTypes.array.isRequired,
  collection: PropTypes.string.isRequired,
};

export default DashboardCardList;
