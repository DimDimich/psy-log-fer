import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

function DashboardCard({ classes, item, title, collection }) {

  return (
    <Link to={`/${collection}/${item.id}`}>
      <Card className={classes.card}>
        <CardContent>
          <Typography className={classes.title} color="textSecondary" gutterBottom>
            {title}
          </Typography>
          <Typography variant="h5" component="h2" color="textPrimary">
            {item.name}
          </Typography>
        </CardContent>
      </Card>
    </Link>
  );
}

DashboardCard.propTypes = {
  classes: PropTypes.object.isRequired,
  item: PropTypes.object.isRequired,
  title: PropTypes.string.isRequired,
  collection: PropTypes.string.isRequired,
};

export default withStyles({
  card: {
    minWidth: '14rem',
    width: '14%',
    float: 'left',
    margin: '12px',
  },
  title: {
    fontSize: 14,
  },
  pos: {
    marginBottom: 12,
  },
})(DashboardCard);
