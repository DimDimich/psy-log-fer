import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Redirect, Link } from 'react-router-dom';

import { withStyles } from "@material-ui/core";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SidebarLinks from '../../constants/SidebarLinks';

function MainNavbar({classes, onMenuClicked}) {
  return (
    <AppBar position="static">
      <Toolbar>

        <IconButton onClick={onMenuClicked}
            color="inherit" aria-label="Menu"
            className={classes.menuButton}>
          <MenuIcon />
        </IconButton>

        <Typography variant="h6" color="inherit"
            className={classes.grow}>
          Psylog
        </Typography>

        {SidebarLinks.options.map((item) => (
          <Link key={item.text} className={classes.link} to={item.to}>
            <Button className={classes.button} variant="text">
              {item.text}
            </Button>
          </Link>
        ))}

        {SidebarLinks.collections.map((item) => (
          <Link key={item.text} className={classes.link} to={item.to}>
            <Button className={classes.button} variant="text">
              {item.text}
            </Button>
          </Link>
        ))}

        <Link className={classes.link} to="/sign-in/">
          <Button className={classes.button} variant="text">
            Sign Out
          </Button>
        </Link>


      </Toolbar>
    </AppBar>
  );
}

const styles = {
  root: {
    flexGrow: 1,
  },
  grow: {
    flexGrow: 1,
  },
  link: {
    textDecoration: 'none',
  },
  button: {
    color: '#fff',
  }
};

export default withStyles(styles)(MainNavbar);

