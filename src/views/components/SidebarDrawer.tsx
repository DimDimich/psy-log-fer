import React, { useState } from 'react';
import Drawer from '@material-ui/core/Drawer';
import PropTypes from 'prop-types';

import { Link } from 'react-router-dom';
import { withStyles, List, ListItem, ListItemIcon, ListItemText, Divider } from '@material-ui/core';

import SidebarLinks from '../../constants/SidebarLinks';

function SidebarDrawer({ classes, onClose, open }) {

  return (
    <Drawer open={open} onClick={onClose} onClose={onClose} variant="temporary">
      <div className={classes.list}>
        <List>
          {SidebarLinks.collections.map((item) => (
            <ListItem button key={item.text}>
              <Link to={item.to}>
                <ListItemText primary={item.text} />
              </Link>
            </ListItem>
          ))}
        </List>
        <Divider/>

        <List>
          {SidebarLinks.options.map((item) => (
            <ListItem button key={item.text}>
              <Link to={item.to} style={{width: '100%', display: 'inline-block'}}>
                <ListItemText primary={item.text} />
              </Link>
            </ListItem>
          ))}
        </List>
      </div>
    </Drawer>
  );
}

SidebarDrawer.propTypes = {
  classes: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  open: PropTypes.bool.isRequired,
};

export default withStyles({
  list: { width: 250 }
})(SidebarDrawer);
