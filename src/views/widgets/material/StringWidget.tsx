import React from 'react';
import TextField from '@material-ui/core/TextField';

export default function StringWidget({ value, onChange, required, disabled, label }) {
  return (
    <TextField
      label={label}
      disabled={disabled}
      required={required}
      value={value || ''}
      onChange={evt => {onChange(evt.target.value)}}
      margin="normal"
    />
  );
};
