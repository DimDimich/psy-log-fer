import StringWidget from './StringWidget';
import RichTextWidget from './RichTextWidget';
import DateWidget from './DateWidget';
import NumberWidget from './NumberWidget';

export default {
  'string': StringWidget,
  'number': NumberWidget,
  'date': DateWidget,
  'rich-text': RichTextWidget,
};
