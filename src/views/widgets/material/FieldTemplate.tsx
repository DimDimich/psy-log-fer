import React from 'react';

export default function FieldTemplate({children}) {

  return (
    <>
      {children}
    </>
  );
}
