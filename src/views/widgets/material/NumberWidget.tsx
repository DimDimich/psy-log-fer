import React from 'react';
import TextField from '@material-ui/core/TextField';

export default function NumberWidget({ value, onChange, required, disabled, label }) {
  return (
    <TextField
      label={label}
      disabled={disabled}
      required={required}
      value={value || 0}
      onChange={evt => {onChange(evt.target.value)}}
      margin="normal"
    />
  );
};
