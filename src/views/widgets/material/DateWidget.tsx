import 'date-fns';
import DateFnsUtils from '@date-io/date-fns';
import React from 'react';
import { MuiPickersUtilsProvider, DatePicker } from 'material-ui-pickers';

export default function DateWidget({ value, onChange, required, disabled, label }) {
  value = value ? value * 1000: Date.now();
  return (
    <MuiPickersUtilsProvider utils={DateFnsUtils}>
      <DatePicker
        margin="normal"
        required={required}
        disabled={disabled}
        label={label}
        value={value}
        onChange={(value) => {onChange(value / 1000)}}
      />
    </MuiPickersUtilsProvider>
  );
};
