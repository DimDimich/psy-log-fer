import React, { useState, useEffect } from 'react';
import withStyles from '@material-ui/core/styles/withStyles';
import mainTag from '../../styles/main';
import { IEntityState, IEntityData } from 'psy-log-models';

import { EntityService } from '../../services/entity.service';
import httpService from '../../services/http.service';
import DashboardCardList from '../containers/DashboardCardList';

function Dashboard ({location, classes}) {
  let params;
  let entity;
  const entityService = new EntityService<IEntityState, IEntityData>(httpService, entity);

  const [state, setState] = useState<IEntityState>({
    count: 0,
    offset: 0,
    total: 0,
    err: undefined,
    items: [],
    selectedItem: undefined,
    collection: '',
  });

  useEffect(() => {
    params = new URLSearchParams(location.search);
    entity = params.get('entity') || 'users';

    entityService
    .setEntity(entity)
    .list()
    .then(({data}) => setState(data));
  }, [location]);

  return (
    <main className={classes.main}>
      <div>
        Dashboard {state.collection}
        <br/>
        Total items: {state.total}
      </div>

      <DashboardCardList items={state.items} collection={state.collection}/>

    </main>
  );
}

// Dashboard.propTypes = {
//   classes: PropTypes.object.isRequired,
// };

export default withStyles(theme => ({
  main: mainTag(theme),
}))(Dashboard);
