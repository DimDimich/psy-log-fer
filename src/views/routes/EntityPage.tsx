import React, { useEffect, useState, BaseSyntheticEvent } from 'react';
import PropTypes from 'prop-types';
import withStyles from '@material-ui/core/styles/withStyles';
import mainTag from '../../styles/main';
import Form from 'react-jsonschema-form';
import httpService from '../../services/http.service';
import { EntityService } from '../../services/entity.service';
import { IEntityState, IEntityData, INote, IUser, ISession } from 'psy-log-models';
import CollectionMapping, { ICollectionMapping } from '../../constants/CollectionMapping';
import FieldTemplate from '../widgets/material/FieldTemplate';
import widgets from '../widgets/material';
import models from '../../constants/schemas';
import * as R from 'ramda';
import { Select, MenuItem, Button } from '@material-ui/core';
import classNames from 'classnames';

function EntityPage({ classes, match }) {
  const [selectedCollection, setSelectedCollection] = useState<ICollectionMapping>(CollectionMapping[0]);
  const [entity, setEntity] = useState<IUser | INote | ISession>(null);
  const entityService = new EntityService<IEntityState, IEntityData>(httpService, null);

  const findCollectionByName = (collectionName) => CollectionMapping.find(item => item.collection === collectionName);
  const findCollectionByModel = (model) => CollectionMapping.find(item => item.model === model);
  const onSelectChange = (evt) => setSelectedCollection(findCollectionByModel(evt.target.value));

  useEffect(() => {
    setSelectedCollection(CollectionMapping[0]);
  }, [match.params.id, match.params.entity]);


  useEffect(() => {
    if (match.params.id) {
      entityService
      .setEntity(match.params.entity)
      .read(match.params.id)
      .then(res => {
        setEntity(res.data.selectedItem);
        setSelectedCollection(findCollectionByName(res.data.collection));
      });
    }
  }, [match.params.id, match.params.entity]);


  const onCreate = ({formData}) => {
    entityService
    .setEntity(selectedCollection.collection)
    .create(formData)
    .then(res => {
      console.log(res.data);
    }, err => {
      console.log(err);
    });
  };


  const onUpdate = ({formData}) => {
    const names = Object.getOwnPropertyNames(models[selectedCollection.model].schema.properties);
    const data = R.pickAll(names, formData);

    entityService
    .setEntity(selectedCollection.collection)
    .update(entity.id, data)
    .then(res => {
      console.log(res.data);
    }, err => {
      console.log(err);
    });
  };


  const renderForm = ({schema, uiSchema}, data) => (
    schema ?
    <Form
      className={classes.form}
      FieldTemplate={FieldTemplate}
      onSubmit={entity && entity.id ? onUpdate : onCreate}
      schema={schema}
      widgets={widgets}
      uiSchema={uiSchema}
      formData={data}
    >
      <Button type="submit" className={classes.offset} variant="contained">
        Save
      </Button>
    </Form>
    : null
  );


  const renderModelSelect = (defaultValue: string, disabled: boolean) => (
    <Select className={classNames(classes.select, classes.offset)} value={defaultValue} onChange={onSelectChange} disabled={disabled}>
      {CollectionMapping.map((item, index) => (
        <MenuItem key={index} value={item.model}>{item.text}</MenuItem>
      ))}
    </Select>
  );


  return (
    <main className={classes.main}>
      {selectedCollection && renderModelSelect(selectedCollection.model, entity && !!entity.id)}
      {selectedCollection && renderForm(models[selectedCollection.model], entity)}
    </main>
  );
}


EntityPage.propTypes = {
  classes: PropTypes.object.isRequired,
};


export default withStyles(theme => ({
  main: mainTag(theme),
  select: {
    width: '10em',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing.unit,
  },
  offset: {
    margin: theme.spacing.unit
  }
}))(EntityPage);
