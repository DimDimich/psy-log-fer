import React, { useState } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import DashboardPage from './views/routes/DashboardPage';
import EntityPage from './views/routes/EntityPage';
import SignInPage from './views/routes/SignInPage';
import NoMatchPage from './views/routes/NoMatchPage';

import { withStyles } from '@material-ui/core';
import SidebarDrawer from './views/components/SidebarDrawer';
import MainNavbar from './views/components/MainNavbar';

function App({ classes }) {

  const [state, setState] = useState({open: false});

  const closeSidebar = () => setState({open: false});
  const openSidebar = () => setState({open: true});

  return (
    <Router>
      <div className={classes.root}>

        <MainNavbar onMenuClicked={openSidebar} />
        <SidebarDrawer onClose={closeSidebar} open={state.open}/>

        <Route path="/dashboard/" component={DashboardPage}/>
        <Route path="/sign-in/" component={SignInPage}/>
        <Route path="/entity/" component={EntityPage}/>
        <Route path="/:entity/:id/" component={EntityPage}/>
        {/* <Route component={NoMatch}/> */}
      </div>
    </Router>
  );
}

const styles = {
  root: {
    flexGrow: 1,
  },
};

export default withStyles(styles)(App);
