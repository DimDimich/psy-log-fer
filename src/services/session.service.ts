import { EntityService } from './entity.service';
import { AxiosInstance } from 'axios';
import { ISessionData, ISessionsState } from 'psy-log-models';
import httpService from './http.service';

class SessionService extends EntityService<ISessionsState,ISessionData>{
  constructor(public http: AxiosInstance) {
    super(http, 'sessions');
  }
}

export default new SessionService(httpService);
