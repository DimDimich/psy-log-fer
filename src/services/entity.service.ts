import { AxiosInstance } from 'axios';

export class EntityService<State, Data> {
  constructor(public http: AxiosInstance, public entity: string) {}

  public list() {
    return this.http.get<State>(`/api/${this.entity}/list?count=10&offset=0`);
  }
  public read(id: number) {
    return this.http.get<State>(`/api/${this.entity}/read/${id}`);
  }

  public create(data: Data) {
    console.log(data);
    return this.http.post<State>(`/api/${this.entity}/create`, {data});
  }

  public update(id: number, data: Data) {
    return this.http.post<State>(`/api/${this.entity}/update/${id}`, data);
  }

  public delete(id: number) {
    return this.http.post<State>(`/api/${this.entity}/delete/${id}`, {});
  }

  public setEntity(e: string) {
    this.entity = e;
    return this;
  }
}
