import { EntityService } from './entity.service';
import { AxiosInstance } from 'axios';
import { INoteData, INotesState } from 'psy-log-models';
import httpService from './http.service';

class NoteService extends EntityService<INotesState,INoteData>{
  constructor(public http: AxiosInstance) {
    super(http, 'notes');
  }
}

export default new NoteService(httpService);
