import { EntityService } from './entity.service';
import { AxiosInstance } from 'axios';
import { IUserData, IUsersState } from 'psy-log-models';
import httpService from './http.service';

class UserService extends EntityService<IUsersState,IUserData>{
  constructor(public http: AxiosInstance) {
    super(http, 'users');
  }
}

export default new UserService(httpService);
