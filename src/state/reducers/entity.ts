import * as Actions from '../actions/entity';

export const initialState = {
  count: 0,
  offset: 0,
  total: 0,
  err: undefined,
  items: undefined,
  selectedItem: undefined,
  collection: undefined,
};

export default function reducer(state = initialState, action) {
  switch(action.type) {
    case Actions.List.type:
    case Actions.Read.type:
    case Actions.Create.type:
    case Actions.Update.type:
    case Actions.Delete.type:
      return {...state, ...action.payload};
  }

  return state;
}

