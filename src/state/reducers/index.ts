import { combineReducers } from 'redux';
import { connectRouter } from 'connected-react-router'
import entity from './entity';

export default (history) => combineReducers({
  router: connectRouter(history),
  entity
})
