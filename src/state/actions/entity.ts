export class List {
  static readonly type = '[Entity] List entities';
  constructor(public entity: string) {}
}

export class Create<Data> {
  static readonly type = '[Entity] Create entity';
  constructor(public entity: string, public payload: Data) {}
}

export class Read {
  static readonly type = '[Entity] Read entity';
  constructor(public entity: string) {}
}

export class Update {
  static readonly type = '[Entity] Update entity';
  constructor(public entity: string) {}
}

export class Delete {
  static readonly type = '[Entity] Delete entity';
  constructor(public entity: string) {}
}
