export default [
  {text: 'User', model: 'UserModel', collection: 'users'},
  {text: 'Note', model: 'NoteModel', collection: 'notes'},
  {text: 'Session', model: 'SessionModel', collection: 'sessions'},
];

export interface ICollectionMapping {
  text: string;
  model: string;
  collection: string;
}
