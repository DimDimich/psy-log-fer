import { UiSchema } from 'react-jsonschema-form';

class SessionUiSchema {
  name: UiSchema = {
    'ui:widget': 'string'
  };

  date: UiSchema = {
    'ui:widget': 'date'
  };

  fee: UiSchema = {
    'ui:widget': 'number'
  };

  room_rent: UiSchema = {
    'ui:widget': 'number'
  };

  therapist_id: UiSchema = {
    'ui:widget': 'number'
  };

  client_id: UiSchema = {
    'ui:widget': 'number'
  };
}

const schema = {
  'properties': {
    'name': {
      'minLength': 3,
      'maxLength': 256,
      'type': 'string'
    },
    'date': {
      'type': 'number'
    },
    'fee': {
      'type': 'number'
    },
    'room_rent': {
      'type': 'number'
    },
    'therapist_id': {
      'type': 'number'
    },
    'client_id': {
      'type': 'number'
    }
  },
  'required': [
    'name',
    'date',
    'fee',
    'room_rent',
    'therapist_id',
    'client_id'
  ],
  'type': 'object'
};

export default {
  uiSchema: new SessionUiSchema(),
  schema
};
