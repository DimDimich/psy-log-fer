import { UiSchema } from 'react-jsonschema-form';

class NoteUiSchema {
  name: UiSchema = {
    'ui:widget': 'string'
  };

  title: UiSchema = {
    'ui:widget': 'string'
  };

  body: UiSchema = {
    'ui:widget': 'rich-text'
  };
}

const schema = {
  'properties': {
    'name': {
      'minLength': 3,
      'maxLength': 256,
      'type': 'string'
    },
    'title': {
      'minLength': 3,
      'maxLength': 256,
      'type': 'string'
    },
    'body': {
      'maxLength': 4096,
      'type': 'string'
    },
  },
  'required': [
    'name',
    'title',
  ],
  'type': 'object'
};

export default {
  uiSchema: new NoteUiSchema(),
  schema
};
