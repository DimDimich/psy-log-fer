import { UiSchema } from 'react-jsonschema-form';

class UserUiSchema {
  name: UiSchema = {
    'ui:widget': 'string'
  };

  address: UiSchema = {
    'ui:widget': 'string'
  };

  birthdate: UiSchema = {
    'ui:widget': 'date'
  };
}

const schema = {
  'properties': {
    'name': {
      'minLength': 3,
      'maxLength': 256,
      'type': 'string'
    },
    'address': {
      'minLength': 3,
      'maxLength': 256,
      'type': 'string'
    },
    'birthdate': {
      'type': 'number'
    }
  },
  'required': [
    'name',
    'address',
    'birthdate'
  ],
  'type': 'object'
};

export default {
  uiSchema: new UserUiSchema(),
  schema,
};
