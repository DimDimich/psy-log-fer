import Note from './Note';
import Session from './Session';
import User from './User';

export default {
  'NoteModel': Note,
  'SessionModel': Session,
  'UserModel': User,
};
