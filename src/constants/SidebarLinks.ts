export default {
  collections: [
    {text: 'Users', to: {pathname: '/dashboard/', search: '?entity=users'}},
    {text: 'Sessions', to: {pathname: '/dashboard/', search: '?entity=sessions'}},
    {text: 'Notes', to: {pathname: '/dashboard/', search: '?entity=notes'}},
  ],
  options: [
    {text: 'Create', to: {pathname: '/entity/'}},
  ]
};
